import React from 'react'
import './App.css'
const Modal = ({ url, closeModal }) => {
  console.log(url)
  return (
    <div className='ModalView'>
      <img src={url} height={450} width={450} />
      <img src="blue_close.jpg" onClick={closeModal} alt="" width={30} height={30} style={{ marginTop: "-10px", marginLeft: '-20px', zIndex: '20000', cursor: "pointer" }} />
    </div>
  )
}

export default Modal
