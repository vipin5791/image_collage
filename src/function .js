import fullUrl, { makeANewUrl } from './configs/config'
import axios from 'axios'

export const callRestApi = ({ url = '', imageUrl = [] }) => {
  let newUrl = fullUrl
  if (url.length) {
    newUrl = makeANewUrl({ text: url })
  }
  return axios.get(newUrl).then((res) => {
    console.log(res)
    let photos = res?.data?.photos.photo
    photos.map((item) => {
      imageUrl.push(`https://live.staticflickr.com/${item.server}/${item.id}_${item.secret}.jpg`)
    })
    console.log(imageUrl)
    return imageUrl

  }).catch((err) => {
    console.log(err)
  })
}

export const getSearchSuggestion = () => {
  let suggestionSearch = localStorage.getItem("searchSuggestion");
  suggestionSearch = suggestionSearch && JSON.parse(suggestionSearch);
  console.log(suggestionSearch)
  return suggestionSearch || [];
};

export const setSearchSuggestionInLocalStorage = ({ searchText }) => {
  let suggestionSearch = getSearchSuggestion();
  if (searchText && !suggestionSearch.includes(searchText)) {
    if (suggestionSearch?.length > 7) {
      suggestionSearch.pop();
    }
    suggestionSearch = [searchText, ...suggestionSearch];
    suggestionSearch = JSON.stringify(suggestionSearch);
    localStorage.setItem("searchSuggestion", suggestionSearch)
  }
};