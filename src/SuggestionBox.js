import React from 'react'

const SuggestionBox = ({ searchSuggestion = [], SearchBySuggestion }) => {
  return (
    <div>
      {searchSuggestion?.map((item) => {
        return <p onClick={() => { SearchBySuggestion({ item }) }} className='suggestion' > {item} < br /></p>
      })}
    </div>
  )
}

export default SuggestionBox
