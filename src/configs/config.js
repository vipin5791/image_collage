const keys = {
  method: "flickr.photos.getRecent",
  api_key: "130d5f856d5fcc5cf0f75b4a6b492e99",
  per_page: "24",
  page: 1,
  format: "json",
  nojsoncallback: "1",
  safe_search: "1",
};
let fullUrl = "https://www.flickr.com/services/rest/?";


Object.keys(keys).forEach((item, index) => {
  if (index !== 0) {
    fullUrl = fullUrl + "&";
  }
  fullUrl = fullUrl + `${item}=${keys[item]}`;
});

export default fullUrl

export const makeANewUrl = ({ text = "" }) => {
  let url = "https://www.flickr.com/services/rest/?";
  if (text) {
    keys.method = 'flickr.photos.search';
    keys.text = text;
  }
  Object.keys(keys).forEach((item, index) => {
    if (index !== 0) {
      url = url + "&";
    }
    url = url + `${item}=${keys[item]}`;
  });
  return url
}
