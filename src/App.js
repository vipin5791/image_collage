import React, { useState, useEffect } from 'react'
import disableScroll from 'disable-scroll';


import './App.css'
import Modal from './modal'
import { callRestApi, getSearchSuggestion, setSearchSuggestionInLocalStorage } from './function ';


function App() {
  let imageUrl = []
  const [imageData, setImageData] = useState([])
  const [searchText, setSearchText] = useState('')
  const [modal, setModal] = useState(false)
  const [suggestion, showSuggestion] = useState(false)
  const [modalImage, setModalImage] = useState('')
  const [page, setPage] = useState(1)
  const [searchSuggestion, setSearchSuggestion] = useState([])

  useEffect(() => {
    callRestApi({ imageUrl }).then((res) => {
      setImageData([...imageData, ...res])
    }).catch((err) => {
      console.log(err)
    })
  }, [page])

  const scrollToEnd = () => {
    setPage(page + 1)
  }

  window.onscroll = function () {
    if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
      scrollToEnd()
    }
  }


  const handleKeyDown = (event) => {
    if (event?.key === 'Enter') {
      setSearchSuggestionInLocalStorage({ searchText })
      callRestApi({ url: searchText, imageUrl: [] }).then((res) => {
        setImageData(res)
      })
      showSuggestion(false)
    }
    window.onscroll = function () {
      if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
        handleKeyDown()
      }
    }
  }

  const changeSearchText = (event) => {
    setSearchText(event.target.value)
  }

  const SearchBySuggestion = ({ item }) => {
    console.log('hello')
    callRestApi({ url: item, imageUrl: [] }).then((res) => {
      let newArray = imageUrl.concat(res)
      imageUrl = newArray
      setImageData([...res])
    })
  }

  const ShowImageModal = ({ url = '' }) => {
    setModal(true)
    setModalImage(url)
    disableScroll.on();
  }
  const CloseImageModal = () => {
    setModal(false)
    setModalImage('')
    disableScroll.off();
  }
  return (
    <div >
      <div className='inputStyle' >
        <h2 style={{ color: 'white' }}>Select Photos</h2>
        <input type='text'
          onBlur={() => {
            showSuggestion(false)
            setSearchSuggestion(getSearchSuggestion())
          }}
          onFocus={() => {
            showSuggestion(true)
            setSearchSuggestion(getSearchSuggestion())
          }}
          onChange={changeSearchText}
          onKeyDown={handleKeyDown}></input>
      </div>
      {console.log(searchSuggestion)}
      <div className='suggestionContainer'>
        <div className='suggestionBox'>
          {suggestion && searchSuggestion && Array.isArray(searchSuggestion) ? searchSuggestion?.map((item) => {
            return <p onClick={() => { SearchBySuggestion({ item }) }} className='suggestion' > {item} < br /></p>
          }) : null}
        </div>
      </div>
      {
        modal ? <Modal url={modalImage} closeModal={CloseImageModal} /> : null
      }
      <div className='collageView'>
        {imageData?.map((item, index) => {
          return (
            <div className='imageView'>
              <img src={item} height={150} width={150} onClick={() => { ShowImageModal({ url: item }) }} />
            </div>
          )
        })}
      </div>
    </div >
  );
}

export default App;
